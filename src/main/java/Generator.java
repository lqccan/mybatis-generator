import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.internal.DefaultShellCallback;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * 使用方法：
 * 1、修改generatorConfig.xml文件中的配置
 *      a、数据库连接、用户名及密码（jdbcConnection标签）
 *      b、model、dao及xml的包和生成地址（targetPackage、targetProject）
 *      c、配置需要生成的表（table标签）
 * 2、运行main方法
 *
 * @author liqingcan
 */
public class Generator {

    public static void main(String[] args) {
        try {
            //获取配置文件
            InputStream is = Generator.class.getClassLoader().getResourceAsStream("generatorConfig.xml");
            //warning信息
            List<String> warnings = new ArrayList<String>();
            //解析配置文件
            Configuration config = new ConfigurationParser(warnings).parseConfiguration(is);
            //生成器（使用覆盖文件模式）
            MyBatisGenerator generator = new MyBatisGenerator(config, new DefaultShellCallback(true), warnings);
            generator.generate(null);
            //打印warning信息
            for (String warning : warnings) {
                System.err.println("> " + warning);
            }
            System.out.println("> 完成");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}