package generator;

import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.internal.util.StringUtility;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Mapper类改名插件
 * @author liqingcan
 */
public class RenameJavaMapperPlugin extends PluginAdapter {

    private String searchString;
    private String replaceString;
    private Pattern pattern;


    public RenameJavaMapperPlugin() {
    }

    @Override
    public boolean validate(List<String> warnings) {

        searchString = properties.getProperty("searchString");
        replaceString = properties.getProperty("replaceString");

        boolean valid = StringUtility.stringHasValue(searchString)
                && StringUtility.stringHasValue(replaceString);

        if (valid) {
            pattern = Pattern.compile(searchString);
        }

        return valid;
    }

    @Override
    public void initialized(IntrospectedTable introspectedTable) {
        String oldType = introspectedTable.getMyBatis3JavaMapperType();
        Matcher matcher = pattern.matcher(oldType);
        oldType = matcher.replaceAll(replaceString);

        introspectedTable.setMyBatis3JavaMapperType(oldType);
    }

}
